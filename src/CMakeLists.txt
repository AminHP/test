configure_file(version.h.in "${CMAKE_CURRENT_BINARY_DIR}/version.h" @ONLY)

add_library(ChillinClient STATIC
	GameClient.h
	GameClient.cpp
	version.h.in
)

set_target_properties(
	ChillinClient PROPERTIES
	PUBLIC_HEADER "GameClient.h;${CMAKE_CURRENT_BINARY_DIR}/version.h"
)

install(
	TARGETS ChillinClient
	EXPORT ChillinClientTargets
	LIBRARY DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
	ARCHIVE DESTINATION "${INSTALL_LIB_DIR}" COMPONENT lib
	RUNTIME DESTINATION "${INSTALL_LIB_DIR}" COMPONENT bin
	PUBLIC_HEADER DESTINATION "${INSTALL_INCLUDE_DIR}/ChillinClient"
	COMPONENT dev
)


# Add dependencies
include(ExternalProject)

set(EXTERNAL_INSTALL_LOCATION ${CMAKE_BINARY_DIR}/external)

ExternalProject_Add(frnetlibExt
    URL https://github.com/Cloaked9000/frnetlib/archive/master.zip
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)

ExternalProject_Add(mbedtlsExt
    URL https://github.com/ARMmbed/mbedtls/archive/mbedtls-2.6.0.zip
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${EXTERNAL_INSTALL_LOCATION}
)

include_directories(${EXTERNAL_INSTALL_LOCATION}/include)
link_directories(${EXTERNAL_INSTALL_LOCATION}/lib)

add_dependencies(ChillinClient frnetlibExt mbedtlsExt)


set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

target_link_libraries(ChillinClient Threads::Threads frnetlib "mbed TLS")
