#include "GameClient.h"

#include <iostream>
#include <thread>

#define SSL_ENABLED
#include <frnetlib/SSLSocket.h>
#include <frnetlib/SSLContext.h>
#include <frnetlib/SSLListener.h>

using namespace std;


void call_from_thread()
{
	cout << "Hello, World" << endl;
}

int send_a_packet(fr::SSLSocket &socket)
{
    std::cout << "Going to send something..." << std::endl;

    //Send the request
    if(socket.send_raw("asd", 3) != fr::Socket::Success)
    {
        //Failed to send packet
        std::cout << "Seems got something wrong when sending" << std::endl;
        return -1;
    }

    //Receive a response
    std::cout << "Waiting for a response..." << std::endl;
    char data[1024];
    size_t dataSize;
    if(socket.receive_raw(data, 1024, dataSize) != fr::Socket::Success)
    {
        std::cout << "Failed to receive server response!" << std::endl;
        return -2;
    }

    std::cout << "Server sent: " << string(data, dataSize) << endl;
    return 0;
}


namespace chillin_client
{

GameClient::GameClient()
{
	thread t(call_from_thread);
	t.join();


	std::shared_ptr<fr::SSLContext> ssl_context(new fr::SSLContext()); //Creates a new 'SSL' context. This stores certificates and is shared between SSL enabled objects.
	//ssl_conext->load_ca_certs_from_file(filepath); //This, or 'load_ca_certs_from_memory' should be called on the context, to load your SSL certificates.

    //Try and connect to the server, create a new TCP object for the job and then connect
    fr::SSLSocket socket(ssl_context);  //Use an fr::SSLSocket if SSL
    if(socket.connect("localhost", "5001") != fr::Socket::Success)
    {
        //Failed to connect
        std::cout << "Failed to connect to: " << "localhost" << ":" << "5001" << std::endl;
        return ;
    }

    //For storing user input and send_a_packet response
    std::string op_str;
    int rtn = 0;

    //Keep going until either we, or the server closes the connection
    while(true)
    {
        //Ask the user what to do
        std::cout << "Choose what you want to do, `c` for `continue`, `q` for `quit`:" << std::endl;
        std::cin >> op_str;
        if(op_str.length() > 1)
        {
            std::cout << "Seems that you inputted more than one character, please retry." << std::endl;
            continue;
        }

        switch(op_str[0])
        {
            case 'c':
                std::cout << "continue" << std::endl;
                rtn = send_a_packet(socket);
                break;
            case 'q':
                break;
            default:
            std::cout << "Invalid input!" << std::endl;
        }

        //Exit/error check
        if(op_str[0] == 'q')
            break;
        if(rtn != 0)
            break;
    }
    
    std::cout << "All done, bye!" << std::endl;
}

GameClient::~GameClient()
{
}

}
